
@extends('layouts.master')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Sticker
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Fixed</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Sticker</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                 @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif  

           
           
            {{ Form::open(array('url' => "/sticker",'name'=>'sticker_form','role'=>'form','files' => true)) }}
            
              <div class="box-body">
                <div class="form-group">
                  {{ Form::label('sticker_name', 'Name') }}
                  {{ Form::text('sticker_name',null,array('class'=>'form-control','id'=>'sticker_name_text')) }}
                 
                 
                </div>
                <div class="form-group">
                  {{ Form::label('sticker_description', 'Description') }}
                  {{ Form::textarea('sticker_description',null,array('class'=>'form-control','id'=>'sticker_description_textarea')) }}
                 
                 
                </div>
                
                <div class="form-group">

                  
                  {{ Form::label('sticker_image', 'Image') }}
                  {{ Form::file('sticker_image', $attributes = array('id'=>'sticker_image_file')) }}
                  

                  <p class="help-block">Please upload image of size XXXX.</p>
                </div>

                <div class="form-group">

                  
                  {{ Form::label('sticker_point', 'Point rquired to achieve sticker') }}
                  {{ Form::text('sticker_point',null,array('class'=>'form-control','id'=>'sticker_point_text')) }}
                  

                  <p class="help-block">Please upload image of size XXXX.</p>
                </div>

                
                <!--<div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>
              </div>-->
              <!-- /.box-body -->

              <div class="box-footer">
                {{ Form::submit('Submit',array('class'=>'btn btn-primary')) }}
                
              </div>
            {{ Form::close() }}
          </div>
         
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  @stop