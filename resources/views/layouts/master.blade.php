@include('headers.master_header')
  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  
@include('sidebars.master_sidebar')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->

  @section('content')
            
        @show
  
  <!-- /.content-wrapper -->
@include('footers.master_footer')
  