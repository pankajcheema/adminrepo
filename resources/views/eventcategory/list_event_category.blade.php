
@extends('layouts.master')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
      <h1>
        Event Category
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Fixed</li>
      </ol>
    </section>
 @if (Session::has('flash_notification.message'))
              <div  id="notify" class="alert alert-success">
                <ul>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                  {{ Session::get('flash_notification.message') }}
              </ul>
              </div>
            @endif  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Event Category</h3>
            </div>
            <!-- /.box-header -->
            <?php $status=array('Inactive','Active'); ?>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($event_categories as $event_category)
                <tr>
                  <td>{{$event_category->cat_name}}</td>
                  <td>{{$event_category->cat_description}}</td>
                  <td><img src="{{ URL::to('/public') }}/uploads/{{$event_category->cat_image}}" height="140px" width="140px"class="img-thumbnail img-responsive" alt="Event Image"></td>
                  <td>{{$status[$event_category->status]}}</td>
                  <td>
                   <a href="{{ URL::to('/event-category')}}/{{$event_category->id}}/edit"><img src="{{ URL::to('/public') }}/images/edit.png" height="20px" width="20px"></a>
                   <a href="javascript:deleteEventCat('{{ $event_category->id }}');"><img src="{{ URL::to('/public') }}/images/remove.png" height="20px" width="20px"></a>
                  </td>
                </tr>
                @endforeach
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>

  @stop