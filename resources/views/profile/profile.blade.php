
@extends('layouts.master')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Profile
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Fixed</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Profile</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                 @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif  

           
           
            {{ Form::model($user,array('url' => '/profile','role'=>'form','files' => true)) }}
            
              <div class="box-body">
                <div class="form-group">
                  {{ Form::label('name', 'Name') }}
                  {{ Form::text('name',null,array('class'=>'form-control','id'=>'exampleInputname')) }}
                 
                 
                </div>
                <div class="form-group">
                  {{ Form::label('email', 'Email') }}
                  {{ Form::email('email',null,array('class'=>'form-control','id'=>'exampleInputEmail1')) }}
                 
                 
                </div>
                <div class="form-group">
                  {{ Form::label('password', 'Password') }}
                  {{ Form::password('password', array('placeholder' => 'Password','class'=>'form-control','id'=>'exampleInputPassword1') ) }}
                 
                </div>  
                <div class="form-group">

                  
                  {{ Form::label('image', 'Profile Image') }}
                  {{ Form::file('image', $attributes = array('id'=>'exampleInputFile')) }}
                  

                  <p class="help-block">Please upload image of size XXXX.</p>
                </div>

                <div class="form-group">
                  <img id="blah" src="{{ URL::to('/public') }}/uploads/{{ Auth::user()->image }}" height="100px" width:"200px"  />
                 </div> 
                <!--<div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div>
              </div>-->
              <!-- /.box-body -->

              <div class="box-footer">
                {{ Form::submit('Submit',array('class'=>'btn btn-primary')) }}
                
              </div>
            {{ Form::close() }}
          </div>
         
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

  @stop