function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#exampleInputFile").change(function(){
    readURL(this);
});

function deleteEventCat(id) {
    if (confirm('Delete this Category?')) {
      var token=$('meta[name="_token"]').attr('content');
        $.ajax({
            type: "POST",
            url: 'event-category/' + id, //resource
            data: {_method: 'delete', _token :token},
            success: function() {
                //if something was deleted, we redirect the user to the users page, and automatically the user that he deleted will disappear
                 window.location = 'event-category';
            }
        });
    }
}

function deletesticker(id) {
    if (confirm('Delete this Sticker?')) {
      var token=$('meta[name="_token"]').attr('content');
        $.ajax({
            type: "POST",
            url: 'sticker/' + id, //resource
            data: {_method: 'delete', _token :token},
            success: function() {
                //if something was deleted, we redirect the user to the users page, and automatically the user that he deleted will disappear
                 window.location = 'sticker';
            }
        });
    }
}