<?php
/*
    *This class is extending the base exception class that hanler.php(App\Exception\Handler);
     basically we are overriding the render fiunction for our custom use



    * PHP 5.6

    * Created By Pankaj Cheema

    * Created_at:22/7/2016

    * Created working with laravel 5.2 stable

    * Updated_at:22/7/16
*/

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\Handler as BaseHandler;



class CustomHandler extends BaseHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        
        
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof HttpException) {
            die('Here HttpException'); //Or do whatever you want
        }
        if ($e instanceof AuthorizationException) {
            die('Here  AuthorizationException'); //Or do whatever you want
        }
        if ($e instanceof ModelNotFoundException) {
            die('Here ModelNotFoundException'); //Or do whatever you want
        }
        if ($e instanceof ValidationException) {
            die('Here ValidationException'); //Or do whatever you want
        }
        $message=$e->getMessage(); 
        return response()->view('error.exception', compact('message'));
    }
}
