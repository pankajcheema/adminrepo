<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class EventCategory extends Moloquent
{
    protected $table='event_category';
    protected $fillable = ['cat_name','cat_description','cat_image'];
}
