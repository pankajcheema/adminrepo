<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

class Sticker extends Moloquent
{
    protected $table='sticker';
    protected $fillable = ['sticker_name','sticker_description','sticker_image','sticker_point'];
}
