<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Requests\StickerRequest;
use App\User;
use App\EventCategory;
use App\Sticker;
use Flash;
use Carbon\Carbon;
use Redirect;
use File;


class StickerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$stickers=Sticker::all();
        return view('sticker.list_sticker',compact('stickers'));   
    }
    public function create()
    {
    	return view('sticker.create_sticker');
    }
    public function store(StickerRequest $request)
    {
    	
	        
	$insertrecord=new Sticker($request->except('_token'));
        if($request->hasFile('sticker_image')) {
                $file = Input::file('sticker_image');
                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                
                $name = $timestamp. '-' .$file->getClientOriginalName();
                
                $insertrecord->sticker_image=$name;

                $file->move(public_path().'/uploads/', $name);
        }
        if($insertrecord->save())
        {
            Flash::success('Sticker saved successfully.');    
            return redirect('/sticker');
        }
        
    }
    public function show()
    {
    	
    }
    public function edit($id)
    {
    	try{    	
    		$sticker=Sticker::find($id);
    		$image=$sticker->cat_image;
    	}catch(\Exception $e){
    		abort(404);	
    	}
        return view('sticker.edit_sticker',compact('sticker','image'));
	
    }
    public function update(StickerRequest $request,$id)
    {
    	
       try{
       $update=Sticker::find($id);
       $data=$request->except('_token','_method');
        if($request->hasFile('sticker_image')) {
                
                $file = Input::file('sticker_image');
                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                
               $name = $timestamp. '-' .$file->getClientOriginalName();
                
                
                $data['sticker_image']=$name;

                $file->move(public_path().'/uploads/', $name);
                File::delete('public/uploads'.$event_cat->cat_image);
        }

       
        if($update->update($data))
        {
            Flash::success('Sticker updated successfully.');    
            return redirect('/sticker');
        }
        }catch(\Exception $e)
        {
            abort(404);
        }
    }
    public function destroy($id)
    {
	try{    	
		$delete=new Sticker();
		if($delete->destroy($id))
		{
		    Flash::success('Sticker deleted successfully.');    
		    
		}
	}catch(\Exception $e){
		abort(404);	
	}
    }

}
