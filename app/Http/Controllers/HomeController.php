<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ProfilePostRequest;
use App\User;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Auth;
use Hash;  
use Redirect;
use App\Http\Repositary\Repositary;
use Flash;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $common; 
    public function __construct(Repositary $common)
    {
        $this->middleware('auth');
        $this->common=$common;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile()
    {

        $user=$this->common->getUser();
        $user=User::findOrfail(Auth::user()->id);
        return view('profile.profile',compact('user'));
    }

    public function postProfile(ProfilePostRequest $request)
    {
        try{
            $user=User::findOrfail(Auth::user()->id);
            if($request->hasFile('image')) {
                $file = Input::file('image');
                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                
                $name = $timestamp. '-' .$file->getClientOriginalName();
                
                $user->image=$name;

                $file->move(public_path().'/uploads/', $name);
            }
            if($request->input('password'))
            {
                $user->password=Hash::make($request->input('password'));
            }
            
            if($user->update($request->only('name','email')))
            {
                Flash::success('Profile Updated Successfully.');    
                return redirect('');

            }
            else{
                die('wrong');
            }
        }catch(\Exception $e)
        {
            die($e->getMessage());
        }
    }
}
