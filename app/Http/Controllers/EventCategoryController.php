<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Requests\EventCategoryRequest;
use App\User;
use App\EventCategory;
use Flash;
use Carbon\Carbon;
use Redirect;
use File;


class EventCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$event_categories=EventCategory::all();
        return view('eventcategory.list_event_category',compact('event_categories'));   
    }
    public function create()
    {
    	return view('eventcategory.create_event_cat');
    }
    public function store(EventCategoryRequest $request)
    {
    	
        $insertrecord=new EventCategory($request->except('_token'));
        if($request->hasFile('cat_image')) {
                $file = Input::file('cat_image');
                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                
                $name = $timestamp. '-' .$file->getClientOriginalName();
                
                $insertrecord->cat_image=$name;

                $file->move(public_path().'/uploads/', $name);
        }
        if($insertrecord->save())
        {
            Flash::success('Event category saved successfully.');    
            return redirect('/event-category');
        }
        
    }
    public function show()
    {
    	
    }
    public function edit($id)
    {
    	$event_cat=EventCategory::find($id);
        $image=$event_cat->cat_image;
        return view('eventcategory.edit_cat_event',compact('event_cat','image'));
    }
    public function update(Request $request,$id)
    {
    	
       try{
       $event_cat=EventCategory::find($id);
       $data=$request->except('_token','_method');
        if($request->hasFile('cat_image')) {
                
                $file = Input::file('cat_image');
                //getting timestamp
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                
               $name = $timestamp. '-' .$file->getClientOriginalName();
                
                
                $data['cat_image']=$name;

                $file->move(public_path().'/uploads/', $name);
                File::delete('public/uploads'.$event_cat->cat_image);
        }

       
        if($event_cat->update($data))
        {
            Flash::success('Event category updated successfully.');    
            return redirect('/event-category');
        }
        }catch(\Exception $e)
        {
            abort(404);
        }
    }
    public function destroy($id)
    {
    	$cat_event=new EventCategory();
        if($cat_event->destroy($id))
        {
            Flash::success('Event category updated successfully.');    
            
        }
    }

}
