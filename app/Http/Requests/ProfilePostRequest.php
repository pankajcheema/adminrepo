<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProfilePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       switch($this->method())
        {
            case 'GET':
            {
                return [];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'email'         => 'required|email|unique:users,id,'.Auth::user()->id,
                    'image'         => 'image',
                    'name'          => 'required',
                ];
            }
            case 'PATCH':
            {
                return [
                    'email'         => 'required|email|unique:users',
                    'name'          => 'required',
                ];
            }
        }
    }
}
