<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'DashboardController@index');
Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);
Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/article', 'DashboardController@getArticle');
//Route::get('/profile', 'HomeController@getProfile');
Route::get('/profile', ['as' => 'profile', 'uses' => 'HomeController@getProfile']);
Route::post('/profile', 'HomeController@postProfile');

Route::resource('event-category', 'EventCategoryController');
Route::resource('sticker', 'StickerController');

Route::group(['prefix' => 'admin'], function () {
    Route::get('users', function ()    {
        // Matches The "/admin/users" URL
    });
});